﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour 
{
	public GridScript gridScript;
	public GameManager gameManager;
	public int value = 0;
	public int index;
	public Material[] materials = null;
	public Material thisMaterial = null;
	public bool hasBeenExtracted = false;
	public bool hasBeenScanned = false;
	public int thisX;
	public int thisY;
	public int thisResourceValue;

	public enum Value
	{
		Max = 0,
		Half = 1,
		Quarter = 2,
		Minimal = 3,
		Unknown = 4
	}

	public enum State
	{
		Clicked,
		Default
	}

	void Awake()
	{
		thisMaterial = materials [3];
	}

	void Start () 
	{
		gameManager = FindObjectOfType<GameManager> ();
		gridScript = FindObjectOfType<GridScript> ();
		this.GetComponent<MeshRenderer>().material = materials [4];
	}

	void Update () 
	{

	}

	public void SetHiddenValue(int value)
	{
		switch(value)
		{
		case 0:
			thisMaterial = materials [0];
			break;
		case 1:
			thisMaterial = materials [1];
			break;
		case 2:
			thisMaterial = materials [2];
			break;
		case 3:
			thisMaterial = materials [3];
			break;
		default:
			Debug.Log ("This tile has no value: " + gameObject.name);
			break;
		}
	}

	public void DesignateMaterials(int resourceValue)
	{
		//resourceValue = Value;

		switch(value)
		{
		case 0:
			GetComponent<MeshRenderer> ().material = materials [0];
			break;
		case 1:
			GetComponent<MeshRenderer> ().material = materials [1];
			break;
		case 2:
			GetComponent<MeshRenderer> ().material = materials [2];
			break;
		case 3:
			GetComponent<MeshRenderer> ().material = materials [3];
			break;
		default:
			Debug.Log ("This tile has no value: " + gameObject.name);
			break;
		}
	}
}
