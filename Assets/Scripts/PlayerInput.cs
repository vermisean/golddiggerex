﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour 
{
	public GameManager gameManager;
	public GridScript gScript;
	public SoundManager sManager; 
	private Camera cam;

	public int currentGoldValue = 0;

	void Start () 
	{
		gameManager = FindObjectOfType<GameManager> ();
		gScript = FindObjectOfType<GridScript> ();
		sManager = FindObjectOfType<SoundManager> ();
		cam = FindObjectOfType<Camera> ();

		currentGoldValue = 0;
	}
	

	void Update () 
	{
		Scan ();
		Extract ();
	}

	public void Scan()
	{
		if (Input.GetMouseButtonUp (0) && GameManager.isScanMode && GameManager.scansRemaining > 0) 
		{ 
			Ray ray = cam.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) 
			{
				if(hit.collider.gameObject.tag == "Tile")
				{
					GameObject thisTile = hit.collider.gameObject;

					sManager.PlayOreSFX ();

					thisTile.GetComponent<TileScript>().hasBeenScanned = true;

					GameManager.scansRemaining--;

					List<GameObject> neighbours = gScript.ScanNeighbours (gScript.tileArray, thisTile.GetComponent<TileScript>().thisX, thisTile.GetComponent<TileScript>().thisY);

					Debug.Log (thisTile.GetComponent<TileScript> ().thisX);
					Debug.Log (thisTile.GetComponent<TileScript> ().thisY);

					thisTile.GetComponent<MeshRenderer> ().material = thisTile.GetComponent<TileScript> ().thisMaterial;

					foreach (GameObject tile in neighbours)
					{
						tile.GetComponent<TileScript> ().hasBeenScanned = true;
						tile.GetComponent<MeshRenderer> ().material = tile.GetComponent<TileScript> ().thisMaterial;
					}

					gameManager.UpdateScans ();
				}
			}
		}
	}


	public void Extract()
	{
		if (Input.GetMouseButtonUp (0) && !GameManager.isScanMode && GameManager.extractsRemaining > 0) 
		{ 
			Ray ray = cam.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) 
			{
				if(hit.collider.gameObject.tag == "Tile" && hit.collider.gameObject.GetComponent<TileScript>().hasBeenScanned && !hit.collider.gameObject.GetComponent<TileScript>().hasBeenExtracted)
				{
					GameObject thisTile = hit.collider.gameObject;

					sManager.PlayScanSFX ();

					thisTile.GetComponent<TileScript>().hasBeenExtracted = true;

					currentGoldValue = currentGoldValue + thisTile.GetComponent<TileScript> ().value;

					GameManager.extractsRemaining--;

					thisTile.gameObject.GetComponent<MeshRenderer> ().material = thisTile.GetComponent<TileScript> ().materials [3];
					List<GameObject> neighbours = gScript.ScanNeighbours (gScript.tileArray, thisTile.GetComponent<TileScript>().thisX, thisTile.GetComponent<TileScript>().thisY);

					foreach(GameObject tile in neighbours)
					{
						tile.GetComponent<TileScript> ().hasBeenExtracted = true;
						tile.GetComponent<MeshRenderer> ().material = tile.GetComponent<TileScript> ().materials [3];
						currentGoldValue += tile.GetComponent<TileScript> ().value;
					}

					gameManager.UpdateExtracts();
				}
			}
		}
	}
}

