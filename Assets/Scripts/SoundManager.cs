﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour 
{
	public AudioSource aSource = null;

	public AudioClip[] scanClips = null;
	public AudioClip[] hitClips = null;

	public AudioClip yeehaw = null;

	void Start () 
	{
		aSource = GetComponent<AudioSource> ();	
	}

	public void PlayScanSFX()
	{
		aSource.PlayOneShot (scanClips [Random.Range (0, scanClips.Length)], 1.0f);
	}

	public void PlayOreSFX()
	{
		aSource.PlayOneShot (hitClips [Random.Range (0, hitClips.Length)], 1.0f);
	}

	public void PlayYeehaw()
	{
		aSource.PlayOneShot (yeehaw, 1.0f);
	}
}
