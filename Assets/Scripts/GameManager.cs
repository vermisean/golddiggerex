﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour 
{
	public PlayerInput pInput;
	public Text helpMessageText = null;
	public Text helpMessageShadowText = null;

	public Text remainingScansText = null;
	public Text remainingScansShadowText = null;

	public Text remainingExtractsText = null;
	public Text remainingExtractsShadowText = null;

	public Text currentGoldText = null;
	public Text currentGoldShadowText = null;

	public Text scanModeText = null;
	public Text scanModeShadowText = null;

	public Button restartButton = null;
	public Button quitButton = null;

	[SerializeField] public static bool isScanMode = true;
	[SerializeField] public static int scansRemaining = 6;
	[SerializeField] public static int extractsRemaining = 3;

	public SoundManager sManager; 

	void Start () 
	{
		pInput = FindObjectOfType<PlayerInput> ();
		scansRemaining = 6;
		extractsRemaining = 3;
		isScanMode = true;
		sManager = FindObjectOfType<SoundManager> ();

		helpMessageText.text = "";
		helpMessageShadowText.text = "";

		remainingScansText.text = "6";
		remainingScansShadowText.text = "6";

		remainingExtractsText.text = "3";
		remainingExtractsShadowText.text = "3";

		currentGoldText.text = "0";
		currentGoldShadowText.text = "0";

		scanModeText.text = "Scan Mode Active";
		scanModeShadowText.text = "Scan Mode Active";

		restartButton.gameObject.SetActive (false);
		quitButton.gameObject.SetActive (false);
	}
	

	void Update () 
	{
		UpdateCurrentGold ();
	}


	public void UpdateCurrentGold()
	{
		currentGoldText.text = pInput.currentGoldValue.ToString();
		currentGoldShadowText.text = pInput.currentGoldValue.ToString();
	}


	public void UpdateScans()
	{
		if(scansRemaining > 0)
		{
			remainingScansText.text = scansRemaining.ToString();
			remainingScansShadowText.text = scansRemaining.ToString();
		}
		else
		{
			remainingScansText.text = scansRemaining.ToString();
			remainingScansShadowText.text = scansRemaining.ToString();
			Debug.Log ("No scans remaining");
			StartCoroutine (DisplayHelpMessage ("You have no scans remaining!", 2.5f));
		}
	}


	public void UpdateExtracts()
	{
		if(extractsRemaining > 0)
		{
			remainingExtractsText.text = extractsRemaining.ToString();
			remainingExtractsShadowText.text = extractsRemaining.ToString();
		}
		else
		{
			sManager.PlayYeehaw ();
			remainingExtractsText.text = extractsRemaining.ToString();
			remainingExtractsShadowText.text = extractsRemaining.ToString();
			StartCoroutine (DisplayHelpMessage ("You have no extracts remaining!\nYour score was: " + pInput.currentGoldValue, 4.5f));
			restartButton.gameObject.SetActive (true);
			quitButton.gameObject.SetActive (true);
		}
	}


	public void UpdateMode()
	{
		isScanMode = !isScanMode;

		if(isScanMode)
		{
			scanModeText.text = "Scan Mode Active";
			scanModeShadowText.text = "Scan Mode Active";
		}
		else
		{
			scanModeText.text = "Extract Mode Active";
			scanModeShadowText.text = "Extract Mode Active";
		}
	}


	IEnumerator DisplayHelpMessage(string helpMessage, float timeToWait)
	{
		helpMessageText.text = helpMessage;
		helpMessageShadowText.text = helpMessage;

		yield return new WaitForSeconds (timeToWait);

		helpMessageText.text = "";
		helpMessageShadowText.text = "";
	}


	public int GetScansRemaining()
	{
		return scansRemaining;
	}


	public bool GetMode()
	{
		return isScanMode;
	}

	public void ResetGame()
	{
		SceneManager.LoadScene (0);
	}

	public void QuitGame()
	{
		Application.Quit ();
	}
}
