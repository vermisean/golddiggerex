﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GridScript : MonoBehaviour 
{
	public GameObject[,] tileArray;
	public GameObject tile = null;
	public float distanceBetweenTiles = 0.6f;
	public int tilesPerRow = 16;
	public int numMaxResources = 6;
	public int maxValue;
	public int halfValue;
	public int quarterValue;
	public int minValue;

	[SerializeField] private int numRows = 16;
	[SerializeField] private int numColumns = 16;

	void Awake()
	{
		maxValue = (int)Random.Range (2000.0f, 5000.0f);
		halfValue = (int)(maxValue / 2.0f);
		quarterValue = (int)(halfValue / 2.0f);
		minValue = (int)(quarterValue / 4.0f);

		GenerateGrid (numRows, numColumns);
	}

	void Start () 
	{
		foreach(GameObject tile in tileArray)
		{
			TileScript thisScript = tile.GetComponent<TileScript> ();
			thisScript.value = minValue;
		}

		AssignMaxResources (numMaxResources, tileArray);
		AssignMaxResources (numMaxResources, tileArray);
		AssignMaxResources (numMaxResources, tileArray);
		AssignMaxResources (numMaxResources, tileArray);
		AssignMaxResources (numMaxResources, tileArray);
	}

	void GenerateGrid(int numberOfRows, int numberOfColumns)
	{
		tileArray = new GameObject[16,16];
		float xOffset = 0.0f;
		float yOffset = 0.0f;
		int indexCount = 0;

		for(int i = 0; i < numberOfColumns; i++)
		{
			for(int j = 0; j < numberOfRows; j++)
			{
				xOffset += distanceBetweenTiles;

				if(j % tilesPerRow == 0)
				{
					yOffset -= distanceBetweenTiles;
					xOffset = 0.0f;
				}

				GameObject gameTile = Instantiate (tile, 
					new Vector3(transform.position.x + xOffset, transform.position.y + yOffset, transform.position.z), 
					transform.rotation);
				
				gameTile.name = "GameTile" + "(" + i.ToString() + "," + j.ToString() + ")";
				gameTile.GetComponent<TileScript> ().thisX = i;
				gameTile.GetComponent<TileScript> ().thisY = j;
				gameTile.transform.SetParent (this.transform);

				AssignValueToTile (gameTile, indexCount);
				indexCount++;
					
				tileArray [i, j] = gameTile;
			}
		}
	}
		

	void AssignValueToTile(GameObject tile, int indexCount)
	{
		TileScript tileScript = tile.GetComponent<TileScript> ();
		tileScript.index = indexCount;
	}


	void AssignMaxResources(int numTiles, GameObject[,] tileArray)
	{
		int randX = (int)Random.Range (2.0f, numRows - 2.0f);
		int randY = (int)Random.Range (2.0f, numColumns - 2.0f);

		SetNeighbours (tileArray, randX, randY);
	}


	void SetNeighbours(GameObject[,] array, int i, int j)
	{
		GameObject thisTile = array [i, j];
		int thisTileIndex = thisTile.GetComponent<TileScript> ().index;

		// 2nd circle
		SetValue(tileArray [i - 2, j - 2], quarterValue, 2);
		SetValue(tileArray [i - 2, j - 1], quarterValue, 2);
		SetValue(tileArray [i - 2, j], quarterValue, 2);
		SetValue(tileArray [i - 2, j + 1], quarterValue, 2);
		SetValue(tileArray [i - 1, j + 2], quarterValue, 2);
		SetValue(tileArray [i - 2, j + 2], quarterValue, 2);
		SetValue(tileArray [i - 1, j - 2], quarterValue, 2);
		SetValue(tileArray [i - 2, j + 2], quarterValue, 2);
		SetValue(tileArray [i, j - 2], quarterValue, 2);
		SetValue(tileArray [i, j + 2], quarterValue, 2);
		SetValue(tileArray [i + 1, j - 2], quarterValue, 2);
		SetValue(tileArray [i + 1, j + 2], quarterValue, 2);
		SetValue(tileArray [i + 2, j - 2], quarterValue, 2);
		SetValue(tileArray [i + 2, j - 1], quarterValue, 2);
		SetValue(tileArray [i + 2, j], quarterValue, 2);
		SetValue(tileArray [i + 2, j + 1], quarterValue, 2);
		SetValue(tileArray [i + 2, j + 2], quarterValue, 2);

		// 1st circle
		SetValue(tileArray [i - 1, j - 1], halfValue, 1);
		SetValue(tileArray [i - 1, j], halfValue, 1);
		SetValue(tileArray [i - 1, j + 1], halfValue, 1);
		SetValue(tileArray [i, j - 1], halfValue, 1);
		SetValue(tileArray [i, j + 1], halfValue, 1);
		SetValue(tileArray [i + 1, j - 1], halfValue, 1);
		SetValue(tileArray [i + 1, j], halfValue, 1);
		SetValue(tileArray [i + 1, j + 1], halfValue, 1);

		// Max tile
		SetValue(tileArray [i, j], maxValue, 0);

	}

	// matIndex legend: 0 = max, 1 = half, 2 = quarter, 3 = min, 4 = unknown
	void SetValue(GameObject tile, int scoreValue, int matIndex)
	{
		TileScript tileScript = tile.GetComponent<TileScript> ();
		tileScript.value = scoreValue;
		tile.GetComponent<TileScript> ().SetHiddenValue (matIndex);
	}

	public List<GameObject> ScanNeighbours(GameObject[,] array, int i, int j)
	{
		List<GameObject> neighbours = new List<GameObject>();

		if(j > 0 && i > 0)
			neighbours.Add(tileArray [i - 1, j - 1].gameObject);	// top L
		if(i > 0 && j > -1)	
			neighbours.Add(tileArray [i - 1, j].gameObject);		// top
		if(j < 15 && i > 0)
			neighbours.Add(tileArray [i - 1, j + 1].gameObject);	// top R
		if(i > -1 && j > 0)
			neighbours.Add(tileArray [i, j - 1].gameObject);		// L
		if(i > -1 && j < 15)
			neighbours.Add(tileArray [i, j + 1].gameObject);		// R
		if(i > -1 && i < 15 && j < 16 && j > 0)
			neighbours.Add(tileArray [i + 1, j - 1].gameObject);	// bottom L
		if(i < 14 && j > -1)
			neighbours.Add(tileArray [i + 1, j].gameObject);		// bottom
		if(i < 15 && j < 15)
			neighbours.Add(tileArray [i + 1, j + 1].gameObject);	// bottom R

		return neighbours;
	}
}
